const _ = require("lodash");
const csv = require("csv");
const moment = require("moment");
const fs = require("fs");

function groupBy(data) {
  const result = {};
  let key = null;
  data.forEach((x) => {
    if (moment(x[0], "dddd M/D/YY").isValid()) {
      key = moment(x[0], "dddd M/D/YY").format();
    }
    if (!key) return;

    if (!result[key]) {
      result[key] = [];
    } else {
      if (key == moment(x[0], "dddd M/D/YY").format()) return;
      result[key].push(x);
    }
  });
  return result;
}

async function main() {
  let result = [];
  const dataOne = [],
    dataTwo = [],
    ordered = {};

  await Promise.all([
    new Promise((resolve) => {
      fs.createReadStream("./files/_1.csv")
        .pipe(csv.parse({ delimiter: ",", from_line: 2 }))
        .on("data", (r) => dataOne.push(r))
        .on("end", () => resolve(dataOne));
    }),
    new Promise((resolve) => {
      fs.createReadStream("./files/_2.csv")
        .pipe(csv.parse({ delimiter: ",", from_line: 2 }))
        .on("data", (r) => dataTwo.push(r))
        .on("end", () => resolve(dataTwo));
    }),
  ]);

  const resultOne = groupBy(dataOne);
  const resultTwo = groupBy(dataTwo);

  _(resultOne)
    .keys()
    .map((key) => {
      if (resultTwo[key]) {
        resultOne[key] = _.concat(resultOne[key], resultTwo[key]);
      }
      return key;
    })
    .orderBy([(key) => new Date(key)], ["asc"])
    .each((key) => (ordered[key] = resultOne[key]));

  _(ordered)
    .keys()
    .each((key) => {
      const keyString = moment(key).format("dddd M/D/YY");
      result.push([keyString]);
      ordered[key].forEach((x) => result.push(x));
    });

  csv.stringify(result).pipe(
    fs.createWriteStream("./files/_r.csv", {
      flags: "w",
      encoding: "utf8",
    })
  );
}

main();
